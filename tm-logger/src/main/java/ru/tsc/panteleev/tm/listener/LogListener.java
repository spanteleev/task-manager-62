package ru.tsc.panteleev.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.ILoggerService;
import ru.tsc.panteleev.tm.service.LoggerService;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public class LogListener implements MessageListener {

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        loggerService.writeLog(textMessage.getText());
    }

}
