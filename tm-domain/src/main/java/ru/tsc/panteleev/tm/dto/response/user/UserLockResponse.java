package ru.tsc.panteleev.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public class UserLockResponse extends AbstractResultResponse {

    public UserLockResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
